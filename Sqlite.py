import sqlite3

#Conectarse a una BD o crearla si no existe
connetion = sqlite3.connect("MiBaseDatos.db")

#para poder interactuar creamos un cursor
cursor = connetion.cursor()

# crear tabla solo si no existe
cursor.execute("Create table if not exists empleados (id int primary key not null, nombre text, sueldo real)")

# #insertar datos
cursor.execute("insert into empleados (id, nombre, sueldo) values (1, 'Miguel', 6000)")
cursor.execute("insert into empleados (id, nombre, sueldo) values (2, 'Daniel', 70000)")

id_empleado = 2
nuevo_nombre = "Daniela"

cursor.execute("update empleados set nombre = ? where id = ?", (nuevo_nombre, id_empleado))

#ejecuta y guarda los cambios
connetion.commit()


#consultar la base de datos
# cursor.execute("select * from empleados")
cursor.execute("select * from empleados where id = ?", (id_empleado,))
empladoBusqueda = cursor.fetchall()

# Capturamos el resultado y guardamos en una variable
# empleados = cursor.fetchall()


# for e in empleados:
#     print(e)

print (empladoBusqueda)



cursor.close()
connetion.close()


