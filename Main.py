from Database import BaseDatos

nombreBD = "MiBaseDatos.db"
baseBD = BaseDatos(nombreBD)

baseBD.conectar()
baseBD.insertarEmpleado("Ana", 150000)

empleados = baseBD.consultarEmpleadosTodos()

for e in empleados:
    print(empleados)

# dato capturado desde consola (x ejemplo)
idempleado = 2

empladoID = baseBD.consultarEmpleadosID(idempleado)

# for eid in empladoID:
#     nombreEmpleado = empladoID[1]

print(f"Nombre Empleado con ID ", idempleado, ": ", empladoID)

baseBD.cerrarConeccion()