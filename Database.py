import sqlite3

class BaseDatos:

    def __init__(self, nombreBD):
        self.nombreBD = nombreBD
        self.connection = None
        self.cursor = None
    
    def conectar(self):
        self.connection = sqlite3.connect("MiBaseDatos.db")
        self.cursor = self.connection.cursor()

    def insertarEmpleado(self, nombre, sueldo):
        self.cursor.execute("Insert into empleados (nombre, sueldo) values (?,?)", (nombre, sueldo))
        self.connection.commit()

    def consultarEmpleadosTodos(self):
        self.cursor.execute("select * from empleados")
        empleados = self.cursor.fetchall()
        return empleados

    def consultarEmpleadosID(self, id):
        self.cursor.execute("select * from empleados where id = ?", (id,))
        empleados = self.cursor.fetchall()
        return empleados

    def cerrarConeccion(self):
        self.cursor.close()
        self.connection.close()